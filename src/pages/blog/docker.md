---
layout: '../../layouts/Post.astro'
title: docker
image: /images/docker
publishedAt: "2023-01-06"
category: 'tools'
---

## Docker
> For running containers

### Install [Docker Desktop](https://docs.docker.com/desktop/install/mac-install/)
